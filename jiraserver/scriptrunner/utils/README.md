## SRRestUtil
This class encapsulate common ways of making pre-authenticated rest requests from ScriptRunner in Jira Server/DC.

You can make pre-authenticated request to the current jira environment as the current user using a [TrustedRequest](https://docs.atlassian.com/sal-trust-api/3.1.2/sal-trust-api/apidocs/com/atlassian/sal/api/net/TrustedRequest.html).

Or you can make a pre-authenticated request to another Atlassian application configured in the Application links and allows impersonation as the current user using a [ApplicationLinkRequest](https://docs.atlassian.com/applinks-api/3.2/com/atlassian/applinks/api/ApplicationLinkRequest.html).

Just provide the end point URL, method, query parameters and body, and the utility will decide if/which request type is best.

```
import jiraserver.scriptrunner.utils.SRRestUtil
def srRestUtil = new SRRestUtil()
srRestUtil.makeRequest('GET', '<currentHost>/rest/api/version/endpoint', [mapKey:mapValue])
```

## ScriptRunnerConfigHelper
A class that can return all the major scriptrunner configurations (notable exception: all configs stored in workflows).
This, in combination with the restEndpoint script scriptRunnerConfigs.groovy can make comparing configurations between two Jira Instances a breeze.
Just run the 2 endpoints on each instance and compare the two outputs using your favorite diffing tool.