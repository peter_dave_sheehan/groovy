package jiraserver.scriptrunner.utils

import com.onresolve.scriptrunner.runner.rest.common.CustomEndpointDelegate
import groovy.transform.BaseScript
import groovy.xml.XmlUtil

import javax.ws.rs.core.MediaType
import javax.ws.rs.core.MultivaluedMap
import javax.ws.rs.core.Response

@BaseScript CustomEndpointDelegate delegate

getAllBehaviourConfigs(httpMethod: "GET", groups: ["jira-system-administrators"]) { MultivaluedMap queryParams, String body ->
    def data = ScriptRunnerConfigHelper.getAllBehaviours()
    XmlSlurper parser = new XmlSlurper()
    try {
        def xml = parser.parseText(data as String)
        log.debug "Xml parsing successful. Serialize and return as xml"
        return Response.ok(XmlUtil.serialize(xml)).type(MediaType.APPLICATION_XML).build()
    } catch (e) {
        log.error "Xml parsing failed. Return as plain text"
        log.error e.message
        return Response.ok(data).type(MediaType.TEXT_PLAIN).build()
    }
}

getScriptRunnerConfigJson(httpMethod: "GET", groups: ["jira-system-administrators"]) { MultivaluedMap queryParams, String body ->
    def jobs = ScriptRunnerConfigHelper.getAllJobs().sort { it.FIELD_NOTES }
    def fragments = ScriptRunnerConfigHelper.getAllFragments().sort { it.FIELD_NOTES }
    def listeners = ScriptRunnerConfigHelper.getAllListeners().sort { it.FIELD_LISTENER_NOTES }
    def endpoints = ScriptRunnerConfigHelper.getAllRestEndpoints() sort { it.FIELD_NOTES }
    return Response.ok([jobs: jobs, listeners: listeners, fragments: fragments, endpoints: endpoints]).build()
}