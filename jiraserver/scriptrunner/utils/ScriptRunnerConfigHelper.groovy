package jiraserver.scriptrunner.utils

import com.onresolve.scriptrunner.db.DatabaseUtil
import groovy.json.JsonSlurper
import groovy.util.logging.Log4j
import groovy.xml.MarkupBuilder
import org.apache.log4j.Level

//import groovy.xml.XmlSlurper //this will be needed in the next version

@Log4j
class ScriptRunnerConfigHelper {
    static String localJiraDbResource = 'local_jira'

    static void setLogLevel(def level) {
        //quick way to turn on logging
        if (level instanceof String) log.setLevel(Level.toLevel(level))
        if (level instanceof Level) log.setLevel(level)
    }

    static def getAllBehaviours() {
        def srRestUtil = new SRRestUtil()
        srRestUtil.setLogLevel('INFO')
        def headers = ['Content-Type': 'application/xml']
        String behaviourEndpoint = '/rest/scriptrunner/behaviours/latest/config'
        def url = "$srRestUtil.baseUrl$behaviourEndpoint"
        log.debug "Getting Behaviour mappings from $url"
        def mappingData = srRestUtil.doRequest('GET', url, null, null, headers) as String
        def writer = new StringWriter()
        def xml = new MarkupBuilder(writer)
        xml.mkp.xmlDeclaration(version: "1.0", encoding: 'utf-8')
        xml.root {
            xml.mkp.yieldUnescaped(mappingData)
            configs {
                log.debug "Collecting behaviour configurations listed in mappings."
                XmlSlurper parser = new XmlSlurper()
                def mappingXml = parser.parseText(mappingData)
                def allConfigNodes = mappingXml.servicedesk.requesttype.@configuration.collect { it.text() } +
                        mappingXml.'*'.@configuration.collect { it.text() } +
                        mappingXml.project.issuetype.@configuration.collect { it.text() }

                allConfigNodes.sort { it as Integer }.unique().each { configId ->
                    url = "$srRestUtil.baseUrl$behaviourEndpoint/$configId"
                    log.debug "Getting behaviour config from $url"
                    def data = srRestUtil.doRequest('GET', url, null, null, headers)
                    if (data) {
                        xml.mkp.yieldUnescaped(data)
                    }
                }
            }
        }
        writer.toString()
    }

    static def getAllJobs() {
        def slurper = new JsonSlurper()
        DatabaseUtil.withSql(localJiraDbResource) { sql ->
            def stashData = sql.rows('Select * from AO_4B00E6_STASH_SETTINGS')
            String jobText = stashData.find { it['KEY'] == 'scheduled_jobs' }['SETTING']
            slurper.parseText(jobText)
        }
    }

    static def getAllFragments() {
        def slurper = new JsonSlurper()
        DatabaseUtil.withSql(localJiraDbResource) { sql ->
            def stashData = sql.rows('Select * from AO_4B00E6_STASH_SETTINGS')
            String fragText = stashData.find { it['KEY'] == 'ui_fragments' }['SETTING']
            slurper.parseText(fragText)
        }
    }

    static def getAllListeners() {
        def slurper = new JsonSlurper()
        DatabaseUtil.withSql(localJiraDbResource) { sql ->
            def select = '''\
                select t.propertyvalue as value
                from propertyentry p 
                inner join propertytext t on t.id=p.id 
                where p.PROPERTY_KEY = 'com.onresolve.jira.groovy.groovyrunner:groovyrunner'
                '''
            def data = sql.firstRow(select)
            String listenerText = data['value']
            slurper.parseText(listenerText)
        }
    }

    static def getAllRestEndpoints() {
        def slurper = new JsonSlurper()
        DatabaseUtil.withSql(localJiraDbResource) { sql ->
            def select = '''\
                select t.propertyvalue as value
                from propertyentry p 
                inner join propertytext t on t.id=p.id 
                where p.PROPERTY_KEY = 'com.onresolve.jira.groovy.groovyrunner:rest-endpoints'
                '''
            def data = sql.firstRow(select)
            String listenerText = data['value']
            slurper.parseText(listenerText)
        }
    }
}
