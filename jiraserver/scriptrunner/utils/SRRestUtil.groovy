package jiraserver.scriptrunner.utils

import com.atlassian.applinks.api.ApplicationLink
import com.atlassian.jira.applinks.JiraApplicationLinkService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.sal.api.net.*
import com.onresolve.scriptrunner.runner.customisers.PluginModule
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import groovy.util.logging.Log4j
import groovyx.net.http.Method
import groovyx.net.http.URIBuilder
import org.apache.log4j.Level

@Log4j
class SRRestUtil {
    String baseUrl = ComponentAccessor.applicationProperties.jiraBaseUrl
    ApplicationUser asUser

    SRRestUtil(ApplicationUser asUser = null) {
        if (!asUser) {
            this.asUser = ComponentAccessor.jiraAuthenticationContext.loggedInUser
        } else {
            this.asUser = asUser
        }
    }

    def doRequest(String methodTypeString, String url, Map queryParams = null, def body = null, headers = [:]) {
        def methodType = Request.MethodType.valueOf(methodTypeString)
        doRequest(methodType, url, queryParams, body, headers)
    }

    def doRequest(Method method, String url, Map queryParams = null, def body = null, headers = [:]) {
        def methodType = Request.MethodType.valueOf(method.toString())
        doRequest(methodType, url, queryParams, body, headers)
    }

    def doRequest(Request.MethodType methodType, String url, Map queryParams = null, def body = null, headers = [:]) {
        def uri = new URIBuilder(url)
        if(queryParams){
            uri.query = queryParams
        }
        def request = getRequest(methodType, uri)
        headers.each { k, v ->
            request.setHeader(k.toString(), v.toString())
        }
        if (body) {
            request.setRequestBody(new JsonBuilder(body).toString())
        } else {
            request.setHeader("Content-Length", "0")
        }
        log.debug "SRRestUtil: executing ${methodType.name()} request at $uri"
        def response = request.executeAndReturn(responseHandler)
        response
    }

    private Request getRequest(Request.MethodType methodType, URIBuilder uri) {
        def request
        def baseUri = new URIBuilder(baseUrl)
        if (uri.host != baseUri.host) {
            def appLinks = ComponentAccessor.getComponent(JiraApplicationLinkService).applicationLinks
            ApplicationLink appLink = appLinks.find { it.rpcUrl.host == uri.host } as ApplicationLink
            assert appLink, "This class can only work to make REST calls to the current app or an application link that allows Authenticated requests (impersonation enabled)"
            request = appLink.createAuthenticatedRequestFactory().createRequest(methodType, uri.toString())
            log.debug "SRRestUtil.getRequest: created an ApplicationLinkRequest"
        } else {
            @PluginModule TrustedRequestFactory trustedRequestFactory
            request = trustedRequestFactory.createTrustedRequest(methodType, uri.toString()) as TrustedRequest
            request.addTrustedTokenAuthentication(baseUri.host, asUser.username)
            log.debug "SRRestUtil.getRequest: created a TrustedRequest"
        }
        return request
    }

    private ReturningResponseHandler responseHandler = new ReturningResponseHandler<Response, Object>() {
        @Override
        Object handle(Response response) throws ResponseException {
            if (response.statusCode != HttpURLConnection.HTTP_OK) {
                throw new Exception(response.responseBodyAsString)
            }
            if(response.headers['Content-Type'] =~ 'application/json') {
                return new JsonSlurper().parseText(response.responseBodyAsString)
            } else {
                return response.responseBodyAsString
            }
        }
    }

    static void setLogLevel(def level) {
        //quick way to turn on logging
        if (level instanceof String) log.setLevel(Level.toLevel(level))
        if (level instanceof Level) log.setLevel(level)
    }
}