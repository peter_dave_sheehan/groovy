package jiraserver.insightUtils.insight

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.exception.CreateException
import com.atlassian.jira.project.Project
import com.atlassian.jira.user.ApplicationUser
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.*
import com.riadalabs.jira.plugins.insight.common.exception.InsightException
import com.riadalabs.jira.plugins.insight.services.events.EventDispatchOption
import com.riadalabs.jira.plugins.insight.services.model.*
import com.riadalabs.jira.plugins.insight.services.model.factory.ObjectAttributeBeanFactory
import com.riadalabs.jira.plugins.insight.services.model.move.MoveAttributeMapping
import com.riadalabs.jira.plugins.insight.services.model.move.MoveObjectBean
import com.riadalabs.jira.plugins.insight.services.model.move.MoveObjectMapping
import groovy.util.logging.Log4j
import org.apache.log4j.Level

@Log4j
final class InsightUtils {
    static ObjectSchemaFacade objectSchemaFacade = ComponentAccessor.getOSGiComponentInstanceOfType(ObjectSchemaFacade)
    static ObjectFacade objectFacade = ComponentAccessor.getOSGiComponentInstanceOfType(ObjectFacade)
    static ObjectTypeFacade objectTypeFacade = ComponentAccessor.getOSGiComponentInstanceOfType(ObjectTypeFacade)
    static ObjectTypeAttributeFacade objectTypeAttributeFacade = ComponentAccessor.getOSGiComponentInstanceOfType(ObjectTypeAttributeFacade)
    static ObjectAttributeBeanFactory objectAttributeBeanFactory = ComponentAccessor.getOSGiComponentInstanceOfType(ObjectAttributeBeanFactory)
    static IQLFacade iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade)
    static ConfigureFacade configureFacade = ComponentAccessor.getOSGiComponentInstanceOfType(ConfigureFacade)

    private InsightUtils() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated")
    }

    static void setLogLevel(def level) {
        //quick way to turn on logging
        if (level instanceof String) log.setLevel(Level.toLevel(level))
        if (level instanceof Level) log.setLevel(level)
    }

    static private ApplicationUser setUser(ApplicationUser asUser = null) {
        def currentUser = ComponentAccessor.jiraAuthenticationContext.loggedInUser
        if (asUser && currentUser != asUser) {
            ComponentAccessor.jiraAuthenticationContext.loggedInUser = asUser
        }
        currentUser
    }

    static private void resetUser(ApplicationUser currentUser) {
        ComponentAccessor.jiraAuthenticationContext.loggedInUser = currentUser
    }

    static EventDispatchOption getDispatchOption(Boolean doDispatch) {
        def dispatchOption = EventDispatchOption.DISPATCH
        if (!doDispatch) {
            dispatchOption = EventDispatchOption.DO_NOT_DISPATCH
        }
        dispatchOption
    }

    static ObjectSchemaBean getObjectSchemaByKey(String schemaKey, ApplicationUser asUser = null) {
        def currentUser = setUser(asUser)
        def schema = objectSchemaFacade.findObjectSchemaBeans().find { it.objectSchemaKey == schemaKey }
        resetUser(currentUser)
        schema
    }

    static ObjectSchemaBean getObjectSchemaFromObject(ObjectBean object, ApplicationUser asUser = null) {
        def currentUser = setUser(asUser)
        def objectType = getObjectTypeFromObject(object)
        def schema = objectSchemaFacade.loadObjectSchema(objectType.objectSchemaId)
        resetUser(currentUser)
        schema
    }

    static ObjectTypeBean getObjectTypeById(Integer objectTypeId, ApplicationUser asUser = null) {
        def currentUser = setUser(asUser)
        try {
            def objectType = objectTypeFacade.loadObjectType(objectTypeId)
            resetUser(currentUser)
            return objectType
        } catch (ignore){
            return null
        } finally {
            resetUser(currentUser)
        }
    }

    static ObjectTypeBean getObjectTypeByName(String schemaKey, String objectTypeName, ApplicationUser asUser = null) {
        def currentUser = setUser(asUser)
        ObjectSchemaBean schema = getObjectSchemaByKey(schemaKey)
        if (!schema) {
            log.error "No schema found for $schemaKey"
            resetUser(currentUser)
            return null
        }
        def objectType = objectTypeFacade.findObjectTypeBeansFlat(schema.id).find { it.name == objectTypeName }
        resetUser(currentUser)
        objectType
    }

    static ObjectTypeBean getObjectTypeByName(ObjectSchemaBean schema, String objectTypeName, ApplicationUser asUser = null) {
        def currentUser = setUser(asUser)
        def objectType = objectTypeFacade.findObjectTypeBeansFlat(schema.id).find { it.name == objectTypeName }
        resetUser(currentUser)
        objectType
    }

    static ObjectTypeBean getObjectTypeFromObject(ObjectBean object, ApplicationUser asUser = null) {
        def currentUser = setUser(asUser)
        def objectType = objectTypeFacade.loadObjectType(object.objectTypeId)
        resetUser(currentUser)
        objectType
    }

    static ArrayList<ObjectTypeAttributeBean> getAttributesForObjectType(ObjectTypeBean objectType, ApplicationUser asUser = null) {
        def currentUser = setUser(asUser)
        def objectTypeAttributes = objectTypeAttributeFacade.findObjectTypeAttributeBeans(objectType.id)
        resetUser(currentUser)
        objectTypeAttributes
    }

    static ObjectBean getObjectByKey(String objectKey, ApplicationUser asUser = null) {
        def currentUser = setUser(asUser)
        def object = objectFacade.loadObjectBean(objectKey)
        resetUser(currentUser)
        object
    }

    static ArrayList<ObjectBean> findObjects(String iql, ApplicationUser asUser = null, Integer limit = null, Integer start = 0) {
        def currentUser = setUser(asUser)
        def objectList = []
        if (limit) {
            objectList = iqlFacade.findObjects(iql, start, limit).objects
        } else {
            objectList = iqlFacade.findObjects(iql)
        }
        resetUser(currentUser)
        objectList
    }

    static ArrayList<ObjectBean> findObjects(Map attributeValueMap, ApplicationUser asUser = null, Integer limit = null, Integer start = 0) {
        def currentUser = setUser(asUser)
        def iql = attributeValueMap.collect { attributeName, value ->
            /"$attributeName" = "$value"/
        }.join(' AND ')
        def objectList = []
        objectList = findObjects(iql, asUser, limit, start)
        resetUser(currentUser)
        objectList
    }

    static ArrayList<ObjectBean> getObjectsByLabel(String objectTypeName, String label, ApplicationUser asUser = null) {
        def currentUser = setUser(asUser)
        def objectList = iqlFacade.findObjects("""objectType = "$objectTypeName" and label ="$label" """)
        resetUser(currentUser)
        objectList

    }

    static ArrayList<ObjectBean> getObjectsByLabel(Integer objectTypeId, String label, ApplicationUser asUser = null) {
        def currentUser = setUser(asUser)
        def objectList = iqlFacade.findObjects("""objectTypeId = $objectTypeId and label ="$label" """)
        resetUser(currentUser)
        objectList
    }

    static Map getAllAttributesAsMap(ObjectBean object, ApplicationUser asUser = null) {
        def currentUser = setUser(asUser)
        def returnMap = object.objectAttributeBeans.collectEntries { objAttr ->
            def attribute = objectTypeAttributeFacade.loadObjectTypeAttribute(objAttr.objectTypeAttributeId)
            def val = getTypedAttributeValues(objAttr, true)
            [(attribute.name): val]
        }
        resetUser(currentUser)
        returnMap
    }
    
    /**
     * @deprecated Use getAllAttributesAsMap() that leverages the new getTypeAttributeValue private method
     */
    @Deprecated
    static Map getAllAttributesAsKeyValuePairs(ObjectBean object, ApplicationUser asUser = null) {
        def currentUser = setUser(asUser)
        StringBuffer output = new StringBuffer()
        def returnMap = object.objectAttributeBeans.collectEntries { objAttr ->
            def attribute = objectTypeAttributeFacade.loadObjectTypeAttribute(objAttr.objectTypeAttributeId)
            def name = attribute.name
            def isMulti = attribute.maximumCardinality != 1
            def val = ''
            ObjectTypeAttributeBean.Type.DEFAULT
            log.info attribute.typeValue
            switch (attribute.type) {
                case ObjectTypeAttributeBean.Type.REFERENCED_OBJECT:
                    val = objAttr.objectAttributeValueBeans.collect { objectFacade.loadObjectBean(it.referencedObjectBeanId).label }
                    break
                case ObjectTypeAttributeBean.Type.STATUS:
                    val = objAttr.objectAttributeValueBeans.collect { configureFacade.loadStatusTypeBean(it.value as Integer).name }
                    break
                case ObjectTypeAttributeBean.Type.USER:
                    val = objAttr.objectAttributeValueBeans.value.collect{ComponentAccessor.userManager.getUserByKey(it as String)}
                    break
                case ObjectTypeAttributeBean.Type.PROJECT:
                    val = objAttr.objectAttributeValueBeans.value.collect{ComponentAccessor.projectManager.getProjectObj(it as Long)}
                    break
                default: //DEFAULT/GROUP
                    val = objAttr.objectAttributeValueBeans.value
                    break
            }
            if(!isMulti && val instanceof List){
                val = val[0]
            }
            [(name): val]
        }
        resetUser(currentUser)
        returnMap
    }
    
    /**
     * This recursive method is ugly and will give you a headache if you try to follow it. But it works
     *
     * @deprecated Use the simpler getObjectAttributeValues()
     * @param object
     * @param dotNotation
     * @param returnObject
     * @param asUser
     * @return Object Dynamically typed groovy object representation of the attribute
     */
    @Deprecated
    static def getAttributeValueFromDotNotation(ObjectBean object, String dotNotation = '', Boolean returnObject = false, ApplicationUser asUser = null) {
        def currentUser = setUser(asUser)
        object = objectFacade.loadObjectBean(object.id)
        if (!object) {
            resetUser(currentUser)
            return null
        }

        log.debug("getAttributeValueFromDotNotation: Getting '${dotNotation ?: 'label'}' from '$object'")
        def objectType = objectTypeFacade.loadObjectType(object.objectTypeId)
        def returnValue
        if (dotNotation == '') {
            if (returnObject) {
                log.trace "getAttributeValueFromDotNotation: returning object $object"
                resetUser(currentUser)
                return object
            } else {
                log.trace "getAttributeValueFromDotNotation: returning label $object.label"
                resetUser(currentUser)
                return object.label
            }
        }
        def parts = dotNotation.split(/\./)
        def previousPartIsNull = false
        parts.each { part ->
            //todo: check the return value when previous part is null
            log.trace "Evaluating part $part. Current returnValue is $returnValue and previousPartIsNull is $previousPartIsNull"
            if (returnValue || previousPartIsNull) {
                log.debug "Cancel processing of part $part"
                return
            }
            def attributeBeanToReturn = objectTypeAttributeFacade.findObjectTypeAttributeBeans(objectType.id).find {
                it.name == part
            }
            if (attributeBeanToReturn) {
                log.trace "getAttributeValueFromDotNotation: $attributeBeanToReturn.name is a valid attribute of $objectType.name"
                def objectAttribute = object.objectAttributeBeans.find { it.objectTypeAttributeId == attributeBeanToReturn.id }
                if (!objectAttribute) {
                    log.debug "getAttributeValueFromDotNotation: $attributeBeanToReturn.name has no value"
                    returnValue = null
                    previousPartIsNull = true
                    return null
                }
                if (attributeBeanToReturn.isObjectReference()) {
                    log.trace "getAttributeValueFromDotNotation: $attributeBeanToReturn.name is a reference object"
                    def referencedObjects = getTypedAttributeValues(objectAttribute)
                    if (referencedObjects) {
                        log.trace "getAttributeValueFromDotNotation: found a value for object reference attribute $attributeBeanToReturn.name"
                        returnValue = referencedObjects.findResults { referencedObject ->
                            getAttributeValueFromDotNotation(referencedObject, parts.drop(1).join('.'), returnObject, asUser)
                        }
                        log.trace "Setting previousPartIsNull to ${(!returnValue)} returnValue =$returnValue"
                        previousPartIsNull = (!returnValue)
                    } else {
                        log.debug "getAttributeValueFromDotNotation: $attributeBeanToReturn.name has no value"
                        returnValue = null
                        previousPartIsNull = true
                    }
                } else {
                    def objectAttributeBeans = object.objectAttributeBeans
                    log.trace "getAttributeValueFromDotNotation: $object has ${objectAttributeBeans.size()} attributes"
                    def attributeBean2 = objectAttributeBeans.find {
                        it.objectTypeAttributeId == attributeBeanToReturn.id
                    }
                    log.debug "getAttributeValueFromDotNotation: attributeBean for attributeId $attributeBeanToReturn.id = $attributeBean2"
                    if (attributeBean2?.objectAttributeValueBeans) {
                        log.trace "getAttributeValueFromDotNotation: attempting to get the value: ${attributeBean2?.objectAttributeValueBeans*.value}"
                        returnValue = getTypedAttributeValues(attributeBean2, true, true)
                    }
                }
            } else {
                log.error "$part is not a valid attribute of $objectType.name\n ${Thread.dumpStack()}"
            }
        }

        log.debug "getAttributeValueFromDotNotation: \n\tobject: $object \n\tattribute: $dotNotation \n\treturning $returnValue"
        resetUser(currentUser)
        returnValue
    }

    /**
     *
     * @deprecated Use the simpler getObjectAttributeValues()
     * @param objects ArrayList<Object> the object to get the attribute from. Object can either be instance of ObjectBean or String objectKey
     * @param dotNotation String DotNotation for the attribute to retreive
     * @param returnObject Boolean. If true and the dotNotation is a an Insight Object reference, the return value will be an ObjectBean. Otherwise, it will be the objectKey
     * @param asUser
     * @return Object ArrayList of dynamically typed groovy object representations of the attribute
     */
    @Deprecated
    static def getAttributeValueFromDotNotation(ArrayList<Object> objects, String dotNotation = '', Boolean returnObject = false, ApplicationUser asUser = null) {
        objects.collect { object ->
            if (object instanceof String || object instanceof GString) {
                object = getObjectByKey(object)
            }
            getAttributeValueFromDotNotation(object, dotNotation, returnObject, asUser)
        }
    }

    /**
     *
     * @deprecated Use the simpler getObjectAttributeValues()
     * @param objectKey String
     * @param dotNotation
     * @param returnObject
     * @param asUser
     * @return
     */
    @Deprecated
    static def getAttributeValueFromDotNotation(String objectKey, String dotNotation = '', Boolean returnObject = false, ApplicationUser asUser = null) {
        def object = getObjectByKey(objectKey)
        getAttributeValueFromDotNotation(object, dotNotation, returnObject, asUser)
    }

    /**
     ** @deprecated Use the simpler getObjectAttributeValues()
     * @param object
     * @param dotNotation
     * @param asUser
     * @return
     */
    @Deprecated
    static ObjectBean getObjectReferenceFromDotNotation(ObjectBean object, String dotNotation = '', ApplicationUser asUser = null) {
        getAttributeValueFromDotNotation(object, dotNotation, true, asUser) as ObjectBean
    }

    /**
     ** @deprecated Use the simpler getObjectAttributeValues()
     * @param objectKey
     * @param dotNotation
     * @param asUser
     * @return
     */
    @Deprecated
    static ObjectBean getObjectReferenceFromDotNotation(String objectKey, String dotNotation = '', ApplicationUser asUser = null) {
        def object = getObjectByKey(objectKey)
        getAttributeValueFromDotNotation(object, dotNotation, true, asUser) as ObjectBean
    }
    
    /**
     * @param returnSingle Return a single value if maxCardinality ==1
     * @param forceReturnSingle Return a single value is size==1 regardless of maxCardinality
     */
    private static def getTypedAttributeValues(ObjectAttributeBean objectAttributeBean, Boolean returnSingle, Boolean forceReturnSingle = false) {
        if (!objectAttributeBean) {
            return null
        }
        def values = getTypedAttributeValues(objectAttributeBean)
        if (returnSingle) {
            def attribute = objectTypeAttributeFacade.loadObjectTypeAttribute(objectAttributeBean.objectTypeAttributeId)
            if (attribute.maximumCardinality == 1 || forceReturnSingle) {
                values = values[0]
                log.debug "getTypedAttributeValues: returning single value $values (${values.getClass()}"
            }
        }
        values
    }
    
    /**
     * Get the attribute values returning an object of the appropriate type based on the attribute type. E.g. User will return List<ApplicationUser>, ObjectReferece will return List<ObjectBean>
     * @param objectAttributeBean
     * @return A list of appropriately typed object
     */
    private static List<Object> getTypedAttributeValues(ObjectAttributeBean objectAttributeBean) {
        if (!objectAttributeBean) {
            return null
        }
        def attribute = objectTypeAttributeFacade.loadObjectTypeAttribute(objectAttributeBean.objectTypeAttributeId)
        if (!objectAttributeBean.objectAttributeValueBeans) {
            log.debug "There are no stored attribute value for $attribute.name for object id $objectAttributeBean.objectId"
            return null
        }
        def typedAttributeValues
        log.debug "getTypedAttributeValues: oa ($objectAttributeBean) is ota ($attribute) type $attribute.type"
        switch (attribute.type) {
            case ObjectTypeAttributeBean.Type.DEFAULT:
                log.debug "getTypedAttributeValues: default type detected. Getting and returning raw value"
                typedAttributeValues = objectAttributeBean.objectAttributeValueBeans*.value
                break
            case ObjectTypeAttributeBean.Type.REFERENCED_OBJECT:
                log.debug "getTypedAttributeValues: object reference detected. Getting and returning objects"
                typedAttributeValues = objectAttributeBean.objectAttributeValueBeans.collect {
                    objectFacade.loadObjectBean(it.referencedObjectBeanId)
                }
                break
            case ObjectTypeAttributeBean.Type.STATUS:
                log.debug "getTypedAttributeValues: status detected. Getting and returning status name"
                typedAttributeValues = objectAttributeBean.objectAttributeValueBeans.collect {
                    configureFacade.loadStatusTypeBean(it.value as Integer).name
                }
                break
            case ObjectTypeAttributeBean.Type.USER:
                log.debug "getTypedAttributeValues: user detected. Getting and returning user objects"
                typedAttributeValues = objectAttributeBean.objectAttributeValueBeans.collect {
                    ComponentAccessor.userManager.getUserByKey(it.value as String)
                }
                break
            case ObjectTypeAttributeBean.Type.PROJECT:
                log.debug "getTypedAttributeValues: project detected. Getting and returning project objects"
                typedAttributeValues = objectAttributeBean.objectAttributeValueBeans.value.collect { ComponentAccessor.projectManager.getProjectObj(it as Long) }
                break
            case ObjectTypeAttributeBean.Type.GROUP:
                log.debug "getTypedAttributeValues: group detected. Getting and returning group objects"
                typedAttributeValues = objectAttributeBean.objectAttributeValueBeans.collect {
                    ComponentAccessor.groupManager.getGroup(it.value as String)
                }
                break
            default:
                log.debug "getTypedAttributeValues: default type detected: returning ${objectAttributeBean.objectAttributeValueBeans*.value} (${objectAttributeBean.objectAttributeValueBeans*.value*.getClass()})"
                typedAttributeValues = objectAttributeBean.objectAttributeValueBeans*.value
                break
        }
        log.debug "getTypedAttributeValues: returing $typedAttributeValues (${typedAttributeValues*.getClass()}"
        typedAttributeValues
    }

    static List<Object> getObjectAttributeValues(ObjectBean object, Object attribute, ApplicationUser asUser = null) {
        getObjectAttributeValues(object, attribute, true, asUser) as List<Object>
    }
    
    /**
     *
     * @param object The insight objectBean that we want attribute values from
     * @param attribute The name or id of the attribute we want returned. Supports "dot notation" to drill down into object references
     * @param alwaysReturnList If false, attributes wil maxCardinality==1 will return only the object. Otherwise, the value will always be a list
     * @param asUser
     * @return
     */
    static def getObjectAttributeValues(ObjectBean object, Object attribute, Boolean alwaysReturnList, ApplicationUser asUser = null) {
        def currentUser = setUser(asUser)
        if ((attribute instanceof String || attribute instanceof GString) && attribute.tokenize('.').size() > 1) {
            log.debug("Getting the first part of Attribute dot notation")
            def dotNotationElements = attribute.tokenize('.')
            def remainingAttribute = dotNotationElements.drop(1).join('.')
            def objectReferences = getObjectAttributeValues(object, dotNotationElements.first() as String, alwaysReturnList, asUser)
            if (objectReferences) {
                log.debug "Collecting $remainingAttribute from $objectReferences"
                if (objectReferences instanceof List) {
                    if (!(objectReferences[0] instanceof ObjectBean)) {
                        throw new InputMismatchException("Attempted to get value for Attribute containing dot notation ($attribute) from $object.objectKey but the first element is not an Object Reference. Returned element: ${objectReferences[0]} (${objectReferences[0].getClass()})")
                    }
                    def collectedReferenceAttributes = objectReferences.collect { ObjectBean refObject -> getObjectAttributeValues(refObject, remainingAttribute, alwaysReturnList, asUser) }
                    log.debug "Collected attributes: $collectedReferenceAttributes"
                    resetUser(currentUser)
                    return collectedReferenceAttributes.flatten().unique()
                } else {
                    if (!(objectReferences instanceof ObjectBean)) {
                        throw new InputMismatchException("Attempted to get value for Attribute containing dot notation ($attribute) from $object.objectKey but the ${dotNotationElements.first()} is not an Object Reference. Returned element: $objectReferences (${objectReferences[0].getClass()})")
                    }
                    def referenceAttributes = getObjectAttributeValues(objectReferences, remainingAttribute, alwaysReturnList, asUser)
                    log.debug "Collected attributes: $referenceAttributes"
                    resetUser(currentUser)
                    return referenceAttributes
                }
            } else {
                log.debug "No value for ${dotNotationElements.first()} on $object.objectKey"
                resetUser(currentUser)
                return null
            }
        } else {
            def attributeToReturn
            if (attribute instanceof Integer) {
                attributeToReturn = objectTypeAttributeFacade.loadObjectTypeAttribute(attribute)
            } else {
                attributeToReturn = objectTypeAttributeFacade.loadObjectTypeAttribute(object.objectTypeId, attribute as String)
            }
            if (!attributeToReturn) {
                log.warn "'$attribute' is not a valid attribute for $object"
                resetUser(currentUser)
                return null
            }
            def objectAttributeBean = objectFacade.loadObjectAttributeBean(object.id, attributeToReturn.id)

            def values = getTypedAttributeValues(objectAttributeBean, !alwaysReturnList)
            log.debug "Returning $attribute (returnList=$alwaysReturnList) from $object: $values (${values.getClass()}"
            resetUser(currentUser)
            return values
        }
    }

    /**
     *  This method assumes the cloned targetObjectType has all the same attributes
     * @param object The source object to be cloned
     * @param targetObjectType The target object type to clone the object to
     * @param excludeAttributes ArrayList of attributes names to exclude from cloning
     * @param overrideAttributes Map of targetAttributeName:value to allow for putting
     *                              specific value/object into the target attribute
     * @return theClonedObject
     */
    static ObjectBean getSimpleObjectClone(
            ObjectBean object,
            ObjectTypeBean targetObjectType,
            ArrayList excludeAttributes = [],
            Map overrideAttributes = [:],
            ApplicationUser asUser = null
    ) {
        def currentUser = setUser(asUser)
        //log.setLevel(Level.DEBUG)
        ArrayList defaultExcludeAttributes = ['Key', 'Created', 'Updated']
        excludeAttributes = (defaultExcludeAttributes + excludeAttributes + overrideAttributes.keySet()).unique()
        log.debug "getSimpleObjectClone: excludeAttributes=$excludeAttributes"
        //create the new object
        def cloneObject = targetObjectType.createMutableObjectBean()

        //transfer all the attributes not in exclude list and not being overridden
        //def newAttributes =[]
        def newAttributes = object.objectAttributeBeans.findAll { objectAttributeBean ->
            def typeAttBean = objectTypeAttributeFacade.loadObjectTypeAttribute(objectAttributeBean.objectTypeAttributeId)
            !(typeAttBean.name in excludeAttributes)
        }.collect { it.createMutable() }

        log.debug "getSimpleObjectClone: overrideAttributes=$overrideAttributes"
        overrideAttributes.each { attributeName, value ->
            log.debug "getSimpleObjectClone: processing overrideAttribute ($attributeName) with overrideValue ($value)"
            def objectTypeAttributeBean = objectTypeAttributeFacade.loadObjectTypeAttribute(targetObjectType.id, attributeName)
            log.debug "getSimpleObjectClone: Identified ota '$objectTypeAttributeBean' from name='$attributeName'"
            log.debug "getSimpleObjectClone: Attempting to create oa using $value (${value.getClass()})"
            newAttributes << objectAttributeBeanFactory.createObjectAttributeBeanForObject(cloneObject, objectTypeAttributeBean, value)
        }
        cloneObject.setObjectAttributeBeans(newAttributes)
        def tmpDisplayAttr = cloneObject.objectAttributeBeans.find { it.objectTypeAttributeId == 217 }

        //log.debug cloneObject.objectAttributeBeans.find{it.objectTypeAttributeId == 217}.objectAttributeValueBeans.first().value
        resetUser(currentUser)
        try {
            //can't validate for some reason unique fields are always failing
            //objectFacade.validateObjectBean(cloneObject)
            return cloneObject
        } catch (exception) {
            log.error "ObjectBean validationfailed", exception
            return null
        }
    }

    static boolean moveObjectAndWait(ObjectBean object, String targetObjectType, Integer maxWaitMillis = 10000, ApplicationUser asUser = null) {
        def currentUser = setUser(asUser)
        def sourceObjectType = objectTypeFacade.loadObjectType(object.objectTypeId)
        def targetObjecType = objectTypeFacade.findObjectTypeBeansFlat(sourceObjectType.objectSchemaId).find { it.name == targetObjectType }
        resetUser(currentUser)
        moveObjectAndWait(object, targetObjecType, maxWaitMillis, asUser)
    }

    static boolean moveObjectAndWait(ObjectBean object, ObjectTypeBean targetObjectType, Integer maxWaitMillis = 10000, ApplicationUser asUser = null) {
        def currentUser = setUser(asUser)
        MoveObjectBean moveObjectBean = new MoveObjectBean()
        moveObjectBean.setObjectSchemaId(targetObjectType.objectSchemaId)
        moveObjectBean.setFromObjectTypeId(object.objectTypeId)
        moveObjectBean.setToObjectTypeId(targetObjectType.id)
        moveObjectBean.setReferences(MoveObjectBean.References.REMOVE_REFERENCES_TO_OBJECT)
        MoveObjectMapping moveMap = new MoveObjectMapping()
        objectTypeAttributeFacade.findObjectTypeAttributeBeans(object.objectTypeId).each { att ->
            def attMap = MoveAttributeMapping.create(att, att)
            log.info "Preparing to move $att (${att.getClass()})"
            moveMap.map(att.id, attMap)
        }

        moveObjectBean.setMapping(moveMap)
        moveObjectBean.setIql("Key = $object.objectKey")

        def moveSuccess = false

        try {
            def moveProgress = objectFacade.moveObjects(moveObjectBean)
            //wait for move to finish
            Integer curWait = 0
            log.debug "moveObjectAndWait: Waiting for move to finish: $curWait ($moveProgress.status)"
            while (moveProgress.isFinished() == false && curWait < maxWaitMillis) {
                Thread.sleep(100)
                curWait = curWait + 100
                log.debug "moveObjectAndWait: Waiting for move to finish: $curWait ($moveProgress.status)"
            }
            if (moveProgress.isFinished()) {
                log.info "moveObjectAndWait: Move complete"
                moveSuccess = true
            } else if (moveProgress.isError()) {
                log.error "moveObjectAndWait: error during move - "
            } else {
                log.warn "moveObjectAndWait: Gave up waiting for move to finish."
            }
        } catch (moveEx) {
            log.error "moveObjectAndWait: Could not move ${object?.objectKey} to ${targetObjectType?.name}: $moveEx.message"
        }
        resetUser(currentUser)
        moveSuccess
    }

    static def deleteObjectByKey(String objectKey, Boolean dispatchEvent = true, ApplicationUser asUser = null) {
        def currentUser = setUser(asUser)
        def object = getObjectByKey(objectKey)
        if (object) {
            objectFacade.deleteObjectBean(object.id, getDispatchOption(dispatchEvent))
        } else {
            log.warn "There are no objects matching $objectKey to delete"
        }
        resetUser(currentUser)
    }

    static def deleteObject(def object, Boolean dispatchEvent = true, ApplicationUser asUser = null) {
        def currentUser = setUser(asUser)
        def objectToDelete = object
        if (object instanceof String || object instanceof GString) {
            objectToDelete = getObjectByKey(object)
        }
        if (objectToDelete) {
            objectFacade.deleteObjectBean(objectToDelete.id, getDispatchOption(dispatchEvent))
        } else {
            log.warn "There are no objects matching $object to delete"
        }
        resetUser(currentUser)
    }

    static Map<String, Object> setObjectAttribute(ObjectBean object, String attributeName, ObjectBean value, Boolean dispatchEvent = true, ApplicationUser asUser = null) {
        setObjectAttribute(object, attributeName, value.id, dispatchEvent, asUser)
    }

    static Map<String, Object> setObjectAttribute(ObjectBean object, String attributeName, Object value, Boolean dispatchEvent = true, ApplicationUser asUser = null) {
        Map<String, Object> returnMap = [success: false, object: object]
        def currentUser = setUser(asUser)
        def objectTypeAttributeBean = objectTypeAttributeFacade.loadObjectTypeAttribute(object.objectTypeId, attributeName)
        def objectAttributeBean = object.createObjectAttributeBean(objectTypeAttributeBean)
        def objectAttributeValueBean = objectAttributeBean.createObjectAttributeValueBean()
        if (value instanceof GString) value = value.toString()
        if (objectTypeAttributeBean.isStatus() && value instanceof String) {
            def status = getStatusByName(getObjectSchemaFromObject(object), value as String)
            value = status.id
        }
        log.debug "Attempting to set value for $attributeName on $object.objectKey using value=$value (${value.getClass()})"
        objectAttributeValueBean.setValue(objectTypeAttributeBean, value)
        objectAttributeBean.setObjectAttributeValueBeans([objectAttributeValueBean])
        try {
            log.debug "Attempting to update $attributeName to $value on $object.objectKey"
            objectAttributeBean = objectFacade.storeObjectAttributeBean(objectAttributeBean, getDispatchOption(dispatchEvent))
            returnMap.success = true
            returnMap.object = objectFacade.loadObjectBean(object.id)
        } catch (e) {
            log.error "Could not set the value($value) for attribute $attributeName on $object.objectKey: $e.message"
            returnMap.error = "Could not set the value($value) for attribute $attributeName on $object.objectKey"
        }
        resetUser(currentUser)
        return returnMap
    }

    static Map<String, Object> setObjectAttribute(ObjectBean object, String attributeName, ArrayList<Object> values, Boolean dispatchEvent = true, ApplicationUser asUser = null) {
        Map<String, Object> returnMap = [success: false, object: object]
        def currentUser = setUser(asUser)
        def objectTypeAttributeBean = objectTypeAttributeFacade.loadObjectTypeAttribute(object.objectTypeId, attributeName)
        def objectAttributeBean = object.createObjectAttributeBean(objectTypeAttributeBean)
        def arrayOfObjectAttValueBeans = values.collect { value ->
            def objectAttributeValueBean = objectAttributeBean.createObjectAttributeValueBean()
            if (value instanceof ObjectBean) {
                objectAttributeValueBean.setValue(objectTypeAttributeBean, value.id)
            } else if (objectTypeAttributeBean.isStatus() && value instanceof String) {
                def status = getStatusByName(getObjectSchemaFromObject(object), value as String)
                objectAttributeValueBean.setValue(objectTypeAttributeBean, status.id)
            } else {
                objectAttributeValueBean.setValue(objectTypeAttributeBean, value)
            }
            objectAttributeValueBean
        }
        objectAttributeBean.setObjectAttributeValueBeans(arrayOfObjectAttValueBeans)
        try {
            log.debug "Attempting to update $attributeName to $values on $object.objectKey"
            objectAttributeBean = objectFacade.storeObjectAttributeBean(objectAttributeBean, getDispatchOption(dispatchEvent))
            returnMap.object = objectFacade.loadObjectBean(object.id)
            returnMap.success = true
        } catch (e) {
            log.error "Could not set the values($values) for attribute $attributeName on $object.objectKey: $e.message"
            returnMap.error = "Could not set the values($values) for attribute $attributeName on $object.objectKey"
        }
        resetUser(currentUser)
        return returnMap
    }

    static Map<String, Object> setObjectAttributes(ObjectBean object, Map attributeUpdateMap, Boolean dispatchEvent = true, ApplicationUser asUser = null) {
        Map<String, Object> returnMap = [success: false, object: object]
        def currentUser = setUser(asUser)
        MutableObjectBean mutableObject = object.createMutable()
        ObjectBean savedObjectBean

        //get all the attributes in the object currently and replace the value (if applicable) with values submitted in the attributeUpdateMap
        def objectAttributeBeans = object.objectAttributeBeans.collect { objectAttribute ->
            def objectTypeAttributeBean = objectTypeAttributeFacade.loadObjectTypeAttribute(objectAttribute.objectTypeAttributeId)
            log.debug "Check if $objectTypeAttributeBean.name exists in ${attributeUpdateMap.keySet()}"
            if (attributeUpdateMap.containsKey(objectTypeAttributeBean.name)) {
                def mObjectAttribute = objectAttribute.createMutable()
                def valueBeans
                def currentValue = objectAttribute.objectAttributeValueBeans*.value
                def newValue = attributeUpdateMap[objectTypeAttributeBean.name]
                if (newValue instanceof GString) newValue = newValue.toString()
                if (objectTypeAttributeBean.isStatus() && newValue instanceof String) {
                    def status = getStatusByName(getObjectSchemaFromObject(object), newValue)
                    newValue = status.id

                }
                if (currentValue == newValue) {
                    log.debug "The new value supplied ($newValue) was the same as the current value ($currentValue)"
                    return objectAttribute
                } else {
                    log.debug "Replacing $objectTypeAttributeBean.name with new value $newValue"
                    if (newValue instanceof List) {
                        valueBeans = newValue.collect { singleValue ->
                            if (singleValue instanceof GString) singleValue = singleValue.toString()
                            def objectAttributeValueBean = mObjectAttribute.createObjectAttributeValueBean()
                            if (singleValue instanceof ObjectBean) {
                                objectAttributeValueBean.setReferencedObjectBeanId(singleValue.id)
                            } else {
                                objectAttributeValueBean.setValue(objectTypeAttributeBean, singleValue)
                            }
                            objectAttributeValueBean
                        }
                    } else {
                        def objectAttributeValueBean = mObjectAttribute.createObjectAttributeValueBean()
                        if (newValue instanceof ObjectBean) {
                            objectAttributeValueBean.setReferencedObjectBeanId(newValue.id)
                        } else {
                            objectAttributeValueBean.setValue(objectTypeAttributeBean, newValue)
                        }
                        valueBeans = [objectAttributeValueBean]
                    }
                    mObjectAttribute.setObjectAttributeValueBeans(valueBeans)
                    //remove the current attribute from the attributeUpdateMap
                    attributeUpdateMap.remove(objectTypeAttributeBean.name)
                    return mObjectAttribute
                }
            } else {
                return objectAttribute
            }
        }
        log.debug "Looping through remaining attributeUpdateMap with keys ${attributeUpdateMap.keySet()}"
        attributeUpdateMap.each { attributeName, value ->
            log.debug "Adding $value to $attributeName for $object"
            def objectTypeAttributeBean = objectTypeAttributeFacade.loadObjectTypeAttribute(object.objectTypeId, attributeName)
            log.debug "objectTypeAttributeBean = $objectTypeAttributeBean"
            def objectAttributeBean = object.createObjectAttributeBean(objectTypeAttributeBean)
            if (value instanceof GString) value = value.toString()
            if (objectTypeAttributeBean.isStatus() && value instanceof String) {
                def status = getStatusByName(getObjectSchemaFromObject(object), value)
                value = status.id
            }
            if (value instanceof List) {
                def valueBeans = value.collect {
                    log.debug "Creating objectAttributeValueBean from $it"
                    def objectAttributeValueBean = objectAttributeBean.createObjectAttributeValueBean()
                    if (it instanceof ObjectBean) {
                        objectAttributeValueBean.setReferencedObjectBeanId(it.id)
                    } else {
                        objectAttributeValueBean.setValue(objectTypeAttributeBean, it)
                    }
                    objectAttributeValueBean
                }
                objectAttributeBean.setObjectAttributeValueBeans(valueBeans)
            } else {
                def objectAttributeValueBean = objectAttributeBean.createObjectAttributeValueBean()
                if (value instanceof ObjectBean) {
                    objectAttributeValueBean.setReferencedObjectBeanId(value.id)
                } else {
                    objectAttributeValueBean.setValue(objectTypeAttributeBean, value)
                }
                objectAttributeBean.setObjectAttributeValueBeans([objectAttributeValueBean])
            }

            objectAttributeBeans << objectAttributeBean
        }
        log.debug "Attempting to store $objectAttributeBeans in $object"
        mutableObject.setObjectAttributeBeans(objectAttributeBeans)
        try {
            /**
             * I used to validate before storing,
             * but validating was somehow triggering the object to be indexed
             * such that non-unique errors would be raised when storing.
             * So I wrapped the operation in a try/catch block instead.
             */
            //objectFacade.validateObjectBean(mutableObject)
            returnMap.object = objectFacade.storeObjectBean(mutableObject, getDispatchOption(dispatchEvent))
            returnMap.success = true
        } catch (e) {
            log.error "Could not update $object.objectKey with $attributeUpdateMap" //$e.message"
            returnMap.error = "Could not update $object.objectKey with $attributeUpdateMap"
        }
        resetUser(currentUser)
        return returnMap
    }

    static Map<String, Object> clearObjectAttribute(ObjectBean object, String attributeName, Boolean dispatchEvent = true, ApplicationUser asUser = null) {
        Map<String, Object> returnMap = [success: false, object: object]
        def dispatchOption = EventDispatchOption.DISPATCH
        if (!dispatchEvent) {
            dispatchOption = EventDispatchOption.DO_NOT_DISPATCH
        }
        def currentUser = setUser(asUser)
        def objectTypeAttributeBean = objectTypeAttributeFacade.loadObjectTypeAttribute(object.objectTypeId, attributeName)
        def objectAttributeBean = object.objectAttributeBeans.find { it.objectTypeAttributeId == objectTypeAttributeBean.id }
        if (objectAttributeBean) {
            try {
                objectFacade.deleteObjectAttributeBean(objectAttributeBean.id as Long, getDispatchOption(dispatchEvent))
                returnMap.object = objectFacade.loadObjectBean(object.id)
                returnMap.success = true
            } catch (e) {
                returnMap.error = "Could not clear attribute $attributeName from $object.objectKey"
                log.error "Could not clear attribute $attributeName from $object.objectKey: $e.message"
            }
        }
        resetUser(currentUser)
        return returnMap
    }

    static ObjectBean storeObject(MutableObjectBean object, Boolean dispatchEvent = true, ApplicationUser asUser = null) {
        def currentUser = setUser(asUser)
        def storedObject = objectFacade.storeObjectBean(object, getDispatchOption(dispatchEvent))
        resetUser(currentUser)
        storedObject
    }

    /**
     * Create an object by passing a map of AttributeName:Attribute value. This will automatically convert ObjectBean,
     * objectKey, ApplicationUser and project Project objects to the correct data type.
     * Can be used recursively in attributes that are reference objects
     *     eg. map=[Name='testname',RefAttr:[Name='refObjName']
     *          If refObjName exists in target object type for RefAttr, it will be linked,
     *          if it doesn't exist, it will be created first, then linked
     * @param objectType
     * @param attributeValueMap
     * @param asUser
     * @return the new object
     */
    static ObjectBean createObject(String schemaKey, String objectTypeName, Map attributeValueMap, Boolean allowObjectReferenceCreate = false, Boolean dispatchEvent = true, ApplicationUser asUser = null) {
        def currentUser = setUser(asUser)
        def schema = getObjectSchemaByKey(schemaKey)
        if (!schema) {
            log.error "No schema found for $schemaKey"
            resetUser(currentUser)
            return null
        }
        def objectTypeBean = getObjectTypeByName(schema, objectTypeName)
        if (!objectTypeBean) {
            log.error "No ObjectType found for $objectTypeName in schema $schemaKey"
        }
        def object = createObject(objectTypeBean, attributeValueMap, allowObjectReferenceCreate, dispatchEvent)
        resetUser(currentUser)
        return object
    }

    static ObjectBean createObject(ObjectSchemaBean schema, String objectTypeName, Map attributeValueMap, Boolean allowObjectReferenceCreate = false, Boolean dispatchEvent = true, ApplicationUser asUser = null) {
        def objectTypeBean = getObjectTypeByName(schema, objectTypeName)
        createObject(objectTypeBean, attributeValueMap, allowObjectReferenceCreate, dispatchEvent, asUser)
    }

    static ObjectBean createObject(ObjectTypeBean objectType, Map attributeValueMap, Boolean allowObjectReferenceCreate = false, Boolean dispatchEvent = true, ApplicationUser asUser = null) {
        def currentUser = setUser(asUser)
        MutableObjectBean object = objectType.createMutableObjectBean()
        def attributeValidationResult = validateAttributes(objectType, attributeValueMap.keySet().asList())
        if (!attributeValidationResult.isValid) {
            throw new CreateException(attributeValidationResult.messages as String)
        }
        def objectTypeAttributes = objectTypeAttributeFacade.findObjectTypeAttributeBeans(objectType.id)
        def oaBeans = attributeValueMap.collect { attributeName, value ->
            def attributeParts = attributeName.tokenize('.')
            def refAttributeName = attributeParts[0]
            def otaBean = objectTypeAttributes.find { it.name == refAttributeName }
            if (otaBean.objectReference && attributeParts.size() > 1) {
                def refObjectType = objectTypeFacade.loadObjectType(otaBean.referenceObjectTypeId)
                // the reference object needs to be associated based on the provided attributes in a dot notation
                // we have to assume that this notation is intended to return a single value, so we'll search for it using IQL
                def refAttributeRemainder = attributeParts.drop(1).join('.')
                def refObjIql = /objectType = "$otaBean.name" and "$refAttributeRemainder" = "$value"/
                def refObjects = iqlFacade.findObjects(refObjIql)
                if (!refObjects) {
                    throw new CreateException(/Unable to find a reference $refObjectType.name object matching '"$refAttributeRemainder" = "$value"' for attribute $otaBean.name/)
                }
                if (refObjects.size() > 1) {
                    value = refObjects
                } else {
                    value = refObjects[0]
                }
            }
            def oaBean = object.createObjectAttributeBean(otaBean)
            log.debug "InsightUtils.createObject: $attributeName = $value"
            def oavBeans = getObjectValueBeans(oaBean, otaBean, value, allowObjectReferenceCreate, dispatchEvent)
            oaBean.setObjectAttributeValueBeans(oavBeans)
            oaBean
        }
        object.objectAttributeBeans = oaBeans
        def createdObject = objectFacade.storeObjectBean(object, getDispatchOption(dispatchEvent))
        resetUser(currentUser)
        return createdObject
    }

    static ObjectTypeAttributeBean getAttributeByName(ObjectBean object, String attributeName, ApplicationUser asUser = null) {
        def objectType = getObjectTypeFromObject(object, asUser)
        getAttributeByName(objectType, attributeName, asUser)
    }

    static ObjectTypeAttributeBean getAttributeByName(ObjectTypeBean objectType, String attributeName, ApplicationUser asUser = null) {
        def currentUser = setUser(asUser)
        ObjectTypeAttributeBean attribute
        try {
            attribute = objectTypeAttributeFacade.loadObjectTypeAttribute(objectType.id, attributeName)
        } catch (InsightException ignored) {
            log.error "No attribute matching $attributeName found in objectType $objectType.name"
            attribute = null
        }
        resetUser(currentUser)
        return attribute
    }

    static StatusTypeBean getStatusByName(ObjectSchemaBean schema, String statusName, ApplicationUser asUser = null) {
        def currentUser = setUser(asUser)
        StatusTypeBean status
        try {
            status = configureFacade.loadStatusTypeBean(schema.id, statusName)
        } catch (InsightException ignored) {
            log.error "No status matching $statusName found in Insight Schema $schema.name"
            status = null
        }
        resetUser(currentUser)
        return status
    }

    private static List<ObjectAttributeValueBean> getObjectValueBeans(ObjectAttributeBean oaBean, ObjectTypeAttributeBean otaBean, Object value, Boolean allowObjectReferenceCreate, Boolean dispatchOption) {
        if (value instanceof List) {
            log.debug "getObjectValueBeans: List detected: $value"
            return value.collect { getObjectValueBean(oaBean, otaBean, it, allowObjectReferenceCreate, dispatchOption) }
        }
        return [getObjectValueBean(oaBean, otaBean, value, allowObjectReferenceCreate, dispatchOption)]
    }

    private static ObjectAttributeValueBean getObjectValueBean(ObjectAttributeBean oaBean, ObjectTypeAttributeBean otaBean, Object value, Boolean allowObjectReferenceCreate, Boolean dispatchOption) {
        log.debug "getObjectValueBean: Start: $value (${value.getClass()}"
        if (value instanceof List) {
            log.debug "getObjectValueBean: List detected: $value "
            return value.collect { getObjectValueBeans(oaBean, otaBean, it, allowObjectReferenceCreate, dispatchOption) }

        }
        log.debug "checking other types: $value (${value.getClass()})"
        def oavBean = oaBean.createObjectAttributeValueBean()
        if (value instanceof ObjectBean) {
            log.debug "ObjectBean detected for: $value (${value.getClass()})"
            oavBean.setReferencedObjectBeanId(value.id)
        } else if (value instanceof ApplicationUser) {
            log.debug "ApplicationUser detected for: $value (${value.getClass()})"
            oavBean.setValue(otaBean, value.key)
        } else if (value instanceof Project) {
            log.debug "Project detected for: $value (${value.getClass()})"
            oavBean.setValue(otaBean, value.id.toInteger())
        } else if (value instanceof Map && otaBean.objectReference) {
            log.debug "Map detected for object reference: $value (${value.getClass()})"
            //in this case the value represents either an existing object or we might need to create a new one
            def findResults = findObjects(value)
            if (findResults) {
                return findResults.collect { getObjectValueBeans(oaBean, otaBean, it, allowObjectReferenceCreate, dispatchOption) }
                //oavBean.setReferencedObjectBeanId(findResults.first().id)
            } else {
                if (!allowObjectReferenceCreate) {
                    throw new CreateException("An attributeValueMap was specified as the value for $otaBean.name but allowObjectReferenceCreate==false. Specifiy the objectBean or the objectKey as the value or ensure the allowObjectReferenceCreate is set to true")
                }
                def objectType = objectTypeFacade.loadObjectType(otaBean.objectTypeId)
                def newRefObject = createObject(objectType, value, dispatchOption)
                if (newRefObject) {
                    oavBean.setReferencedObjectBeanId(newRefObject.id)
                }
            }
        } else if (value instanceof String && otaBean.objectReference) {
            log.debug "String detected for object reference: $value (${value.getClass()})"
            def refObject = getObjectByKey(value)
            if (refObject) {
                oavBean.setReferencedObjectBeanId(refObject.id)
            }
        } else if (value instanceof String && otaBean.status) {
            def objectType = objectTypeFacade.loadObjectType(otaBean.objectTypeId)
            def schema = objectSchemaFacade.loadObjectSchema(objectType.objectSchemaId)
            def status = getStatusByName(schema, value as String)
            oavBean.setValue(otaBean, status.id)
        } else {
            def ot = objectTypeFacade.loadObjectType(otaBean.objectTypeId)
            log.debug "Nothing detected for $otaBean.name:  $value (${value.getClass()}) AttributeType: $otaBean.type-$otaBean.defaultType ($ot)"
            oavBean.setValue(otaBean, value)
        }
        oavBean
    }

    private static Map<String, Object> validateAttributes(ObjectTypeBean objectType, List attributeList, attributeValidationResults = [:]) {
        def objectTypeAttributes = objectTypeAttributeFacade.findObjectTypeAttributeBeans(objectType.id)
        if (!attributeValidationResults) {
            attributeValidationResults = [isValid: true, messages: [], invalidAttributeNames: []]
        }
        attributeList.each { String attributeName ->
            def attributeParts = attributeName.tokenize('.')
            if (!objectTypeAttributes*.name.contains(attributeParts[0])) {
                attributeValidationResults.isValid = false
                attributeValidationResults.invalidAttributeNames << attributeName
                attributeValidationResults.messages << "$attributeName is not valid for $objectType.name"
            }
            if (attributeParts.size() > 1) {
                def refAttr = objectTypeAttributes.find { it.name == attributeParts[0] }
                if (!refAttr.objectReference) {
                    attributeValidationResults.isValid = false
                    attributeValidationResults.invalidAttributeNames << attributeParts[0]
                    attributeValidationResults.messages << "${attributeParts[0]} is not valid refernce attribute and can't specify sub-attributes: ${attributeParts.drop(1)}"
                }
                def refObjectType = objectTypeFacade.loadObjectType(refAttr.referenceObjectTypeId)
                attributeValidationResults = validateAttributes(refObjectType, attributeParts.drop(1), attributeValidationResults)
            }
        }
        attributeValidationResults
    }

    static def withIql(String iql, ApplicationUser asUser, Closure closure) {
        def currentUser = setUser(asUser)
        try {
            withIql(iql, closure)
        } finally {
            resetUser(currentUser)
        }
    }

    static def withIql(String iql, Closure closure) {
        iqlFacade.validateIQL(iql)
        def pageSize = 200
        def start = 0
        def closureReturnValues = []
        while (true) {
            log.debug "witIql chunk: start=$start size=$pageSize"
            def objects = iqlFacade.findObjects(iql, start, pageSize).objects
            if (objects) {
                objects.each { object ->
                    closureReturnValues << closure.call(object)
                }
            } else {
                break
            }
            start = start + pageSize
        }
        closureReturnValues
    }
}